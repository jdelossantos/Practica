import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {ave} from './ave.model';
import {aveb} from './aveb.model';

@Injectable()
export class PeticiongetService {
  getBirdsUrl: string = "http://dev.contanimacion.com/birds/public/getBirds/SmF2aWVyRGUgbG9zIFNhbnRvcw==";
  addBirdsUrl: string = "http://dev.contanimacion.com/birds/public/addBird/"
  private ave: Array<ave>;

  constructor(public http:HttpClient) {  
   }
   
   postRespuesta(_body: aveb): Observable<ave> {
    let json= JSON.stringify(_body);
    let params= "json="+json;
    return this.http.post<ave>(this.addBirdsUrl, _body);
  }

  getRespuesta(): Observable<ave[]> {
    return this.http.get<ave[]>(this.getBirdsUrl);
  }
  putRespuesta(_id: number, _body: aveb): Observable<ave> {
    let urlcom = this.addBirdsUrl + "/" + _id;
    return this.http.put<any>(urlcom, _body);
  }
}

