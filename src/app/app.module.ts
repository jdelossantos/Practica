import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AgmCoreModule } from '@agm/core';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormularioaccesoComponent } from './formularioacceso/formularioacceso.component';
import { MenuprincipalComponent } from './menuprincipal/menuprincipal.component';
import { ListadoavesComponent } from './listadoaves/listadoaves.component';
import { DetalleaveComponent } from './detalleave/detalleave.component';
import { AñadiraveComponent } from './añadirave/añadirave.component';
import { AñadiravistamientoComponent } from './añadiravistamiento/añadiravistamiento.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { APP_ROUTING } from './app.routes';
import { PeticiongetService } from './peticionget.service';
import { DetalleserviceService } from './detalleservice.service';
import { LoginGuard } from './login.guard';
import { NoLoginGuard } from './no-login.guard';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuprincipalComponent,
    ListadoavesComponent,
    DetalleaveComponent,
    AñadiraveComponent,
    AñadiravistamientoComponent,
    FormularioaccesoComponent,
    UsuarioComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    APP_ROUTING,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBFbzRtzsHS80HkgIubA8OMMgzeSPEbDlk'
    })
  ],
  providers: [PeticiongetService,DetalleserviceService,LoginGuard,NoLoginGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
