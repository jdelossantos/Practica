import { TestBed, inject } from '@angular/core/testing';

import { PeticiongetService } from './peticionget.service';

describe('PeticiongetService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PeticiongetService]
    });
  });

  it('should be created', inject([PeticiongetService], (service: PeticiongetService) => {
    expect(service).toBeTruthy();
  }));
});
