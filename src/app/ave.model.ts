export interface ave{
  
    bird_name: string;
    bird_image: string;
    bird_sightings: string;
    sighting_list:{
        id: string;
        idAve: string;
        place: string;
        long: string;
        lat: string;
    }
        
    
    mine: number;
}