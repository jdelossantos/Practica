import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

interface LoginResponse {
  status: string
  id: string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Ornitología.com';
  loading: boolean;

  
  constructor (private router: Router){ 
    this.loading = true;

  }
  
  ngOnInit(
  ) {
    this.loading = false;

  }
  
}
