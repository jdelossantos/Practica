import { TestBed, inject } from '@angular/core/testing';

import { DetalleserviceService } from './detalleservice.service';

describe('DetalleserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DetalleserviceService]
    });
  });

  it('should be created', inject([DetalleserviceService], (service: DetalleserviceService) => {
    expect(service).toBeTruthy();
  }));
});
