export class detalleaveb {
    id: number;
    bird_image: string;
    bird_name: string;
    bird_sightings: string;
    sightings_list:{
        id:string;
        idAve:string;
        place:string;
        long:string;
        lat:string;}
}