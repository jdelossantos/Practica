import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Subscriber } from 'rxjs/Subscriber';
import { Injectable } from '@angular/core';
import { PeticiongetService } from '../peticionget.service'
import {HttpClient} from '@angular/common/http'
import {detalleave} from '../detalleave.model'
import {detalleaveb} from '../detalleaveb.model'
import { ROUTER_CONFIGURATION, Router } from '@angular/router';
import { DetalleserviceService } from '../detalleservice.service';

@Component({
  selector: 'app-detalleave',
  templateUrl: './detalleave.component.html',
  styleUrls: ['./detalleave.component.css']
})
export class DetalleaveComponent implements OnInit {
  getdetalleUrl: string = "http://dev.contanimacion.com/birds/public/getBirdDetails/1";
  detalleave = new detalleaveb();
  resultado:Array<detalleave>;
  id:number;
  bird_sightings:Array<any>;
  labelOptions: string;
  lat: number = 39.4968782;
  lng: number = -0.4454402;


  constructor(private http:HttpClient,
    private detalleaveservice:DetalleserviceService,private router:Router) { }

  ngOnInit() {
    this.getDetalle(1);
    this.getPeticion(1);
  }
  getDetalle(respId:number) {
    this.http.get<any[]>(this.getdetalleUrl+respId)
      .subscribe( 
        data => {
          console.log("////////////////");
          console.log("GET BIRD");
          console.log("DATA:"+respId);
        },
        
        err => {
          console.log("Error en la petición.")
        }
      );   
  }
  getPeticion(detalleaveId:number) {
    this.detalleaveservice.getRespuesta()
    .subscribe(
      data => {
        this.resultado = data;
        console.log("ok")
    },
      err => {
        console.log(err);
      }
    );
  }
  postSentServices(body: detalleaveb) {
    this.detalleaveservice.postRespuesta(body).subscribe(
      data => {
        this.resultado = [];
        this.resultado.push(data);
      },
      err => {}
    );

  }
}
