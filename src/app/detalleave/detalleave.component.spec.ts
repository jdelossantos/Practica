import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleaveComponent } from './detalleave.component';

describe('DetalleaveComponent', () => {
  let component: DetalleaveComponent;
  let fixture: ComponentFixture<DetalleaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
