import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {detalleave} from './detalleave.model'
import {detalleaveb} from './detalleaveb.model'

@Injectable()
export class DetalleserviceService {
getdetailUrl: string = "http://dev.contanimacion.com/birds/public/getBirdDetails/1";
private detalleave: Array<detalleave>;

  constructor(public http:HttpClient) { }
  postRespuesta(_body: detalleaveb): Observable<any> {

    return this.http.post<detalleave>(this.getdetailUrl, _body);
  }
  getRespuesta(): Observable<detalleave[]> {
    return this.http.get<detalleave[]>(this.getdetailUrl);
  }
}
