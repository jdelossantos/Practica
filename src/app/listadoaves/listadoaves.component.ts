import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Subscriber } from 'rxjs/Subscriber';
import { Injectable } from '@angular/core';
import { PeticiongetService } from '../peticionget.service'
import {HttpClient} from '@angular/common/http'
import {ave} from '../ave.model'
import {aveb} from '../aveb.model'
import { ROUTER_CONFIGURATION, Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-listadoaves',
  templateUrl: './listadoaves.component.html',
  styleUrls: ['./listadoaves.component.css']
})
export class ListadoavesComponent implements OnInit {
  getBirdsUrl: string = "http://dev.contanimacion.com/birds/public/getBirds/userId?";
  ave = new aveb();
  resultado:Array<ave>;
  id:number;
  detalle:Array<any>;


  constructor(private http:HttpClient,
    private peticionget:PeticiongetService,
    private router:Router,private route:ActivatedRoute) {
    }

  ngOnInit() {
    console.log("INIT");
    this.getBirds(1);
    this.getPeticion(1);
  }
  
  getBirds(userId:number) {
    this.http.get<any[]>(this.getBirdsUrl+userId)
      .subscribe( 
        data => {
          console.log("////////////////");
          console.log("GET BIRDS");
          console.log("Recuperamos la respuesta de la petición getBirds (podemos ver el detalle y estructura vía Postman):")
          console.log("DATA:"+data);
          console.log("Viendo que se trata de un array de objetos podemos recuperar así los datos de las diferentes aves haciendo un bucle");
          data.forEach(ave => {
            console.log("----------------");
            console.log( "Id del ave: "+ ave.id );
            console.log( "Nombre del ave: "+ ave.bird_name );
            console.log( "Url imagen ave: "+ ave.bird_image );
            console.log( "Avistamientos: "+ ave.bird_sightings );
            console.log( "Mine: "+ ave.mine );

          });
        },
        
        err => {
          console.log("Error en la petición.")
        }
      );   
  }
  getPeticion(userId:number) {
    this.peticionget.getRespuesta().subscribe(
      data => {
        this.resultado = data;
      },
      err => {
        console.log(err);
      }
    );
  }
  postSentServices(body: aveb) {
    this.peticionget.postRespuesta(body).subscribe(
      data => {
        this.resultado = [];
        this.resultado.push(data);
      },
      err => {}
    );
  }
  
}
