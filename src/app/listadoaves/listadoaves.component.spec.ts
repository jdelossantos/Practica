import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoavesComponent } from './listadoaves.component';

describe('ListadoavesComponent', () => {
  let component: ListadoavesComponent;
  let fixture: ComponentFixture<ListadoavesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoavesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoavesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
