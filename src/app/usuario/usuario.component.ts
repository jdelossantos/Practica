import { Component, OnInit } from '@angular/core';
import { ROUTER_CONFIGURATION, Router } from '@angular/router';
import { FormsModule} from '@angular/forms';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  constructor(
    public id: string,
    public password: string,
  ) { }

  ngOnInit() {
  }

}
