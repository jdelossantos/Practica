export class aveb {
    public id: number;
    public bird_name: string;
    public bird_description: string;
    public bird_image: string;
    public bird_sightings: string;
    public sighting_list:{
        id: string;
        idAve: string;
        place: string;
        long: string;
        lat: string;
    }
    public mine: number;
}
