import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AñadiraveComponent } from './añadirave.component';

describe('AñadiraveComponent', () => {
  let component: AñadiraveComponent;
  let fixture: ComponentFixture<AñadiraveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AñadiraveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AñadiraveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
