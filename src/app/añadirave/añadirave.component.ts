import { Component, OnInit } from '@angular/core';
import {PeticiongetService} from '../peticionget.service';
import { ave } from '../ave.model';
import { aveb } from '../aveb.model';
import {FormControl, Validators} from '@angular/forms';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ROUTER_CONFIGURATION, Router } from '@angular/router';



@Component({
  selector: 'app-añadirave',
  templateUrl: './añadirave.component.html',
  styleUrls: ['./añadirave.component.css']
})
export class AñadiraveComponent implements OnInit {
ContactModel= new aveb();
peticion= new aveb();
peticionPut= new aveb();
resultado: Array<ave>;
id: number= 1;

  constructor(private peticionget: PeticiongetService, private router: Router) { }
  ngOnInit() {
  }
  
  onSubmit(tipo: number) {
    this.router.navigate(['/listadoaves']);
    console.log("ok",this.ContactModel);
    switch ( tipo ) {
      case 1:
      this.postSentServices(this.ContactModel);
      break;
      case 2:
      if(this.peticionPut.bird_name == null && this.peticionPut.bird_description == null){
        alert("llene todo los campos de put");

      }
      break;
    }
}
onchange($event) {
  this.id = $event.target.value;

}
getPeticion() {
  this.peticionget.getRespuesta().subscribe(
    data => {
      this.resultado = data;
      console.log("muy bien");
    },
    err => {
      console.log(err);
    }
  );
}
postSentServices(body: aveb) {
  this.peticionget.postRespuesta(body).subscribe(
    data => {
      this.resultado = [];
      this.resultado.push(data);
    },
    err => {"no lo has hecho bien"}
  );
}
putSentServices(body: aveb, id: number) {
  this.peticionget.putRespuesta(id, body).subscribe(
    data => {
      this.resultado = [];
      this.resultado.push(data);
    },
    err => {"no lo has hecho bien"}
  );
}
}
