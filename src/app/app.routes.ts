
import { LoginComponent } from './login/login.component';
import { MenuprincipalComponent } from './menuprincipal/menuprincipal.component';
import { ListadoavesComponent } from './listadoaves/listadoaves.component';
import { DetalleaveComponent } from './detalleave/detalleave.component';
import { AñadiraveComponent } from './añadirave/añadirave.component';
import { AñadiravistamientoComponent } from './añadiravistamiento/añadiravistamiento.component';
import { FormularioaccesoComponent} from './formularioacceso/formularioacceso.component';
import { UsuarioComponent } from './usuario/usuario.component';

import {RouterModule, Routes} from "@angular/router";
import { LoginGuard } from './login.guard';
import { NoLoginGuard } from './no-login.guard';

const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'menuprincipal', component: MenuprincipalComponent, canActivate:[LoginGuard] },
  { path: 'listadoaves', component: ListadoavesComponent, canActivate:[LoginGuard] },
  { path: 'detalleave', component: DetalleaveComponent, canActivate:[LoginGuard] },
  { path: 'añadirave', component: AñadiraveComponent, canActivate:[LoginGuard] },
  { path: 'añadiravistamiento', component: AñadiravistamientoComponent, canActivate:[LoginGuard] },
  { path: 'detalleave/:id', component: DetalleaveComponent, canActivate:[LoginGuard]}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);