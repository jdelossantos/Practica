import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AñadiravistamientoComponent } from './añadiravistamiento.component';

describe('AñadiravistamientoComponent', () => {
  let component: AñadiravistamientoComponent;
  let fixture: ComponentFixture<AñadiravistamientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AñadiravistamientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AñadiravistamientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
