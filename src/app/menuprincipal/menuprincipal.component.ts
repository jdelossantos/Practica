import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from "@angular/router";
import { NgForm }    from '@angular/forms';

interface LoginResponse {
  status: string
  id: string
  password: string
}


@Component({
  selector: 'app-menuprincipal',
  templateUrl: './menuprincipal.component.html',
  styleUrls: ['./menuprincipal.component.css']
})
export class MenuprincipalComponent implements OnInit {
  loginUrl: string = "http://dev.contanimacion.com/birds/public/login/";
 
  
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    console.log("INIT");
    this.login("Javier","De los Santos");
  }
  login(user:string,password:string) {
    this.http.post<LoginResponse>(this.loginUrl,{"user": user, "password": password})
    .subscribe( 
      data => {
        console.log("////////////////");
        console.log("LOGIN");
        console.log("Login status: " + data.status);
        console.log("Id del usuario: "+ data.id );
      },
      err => {
        console.log("Error en la petición.")
      }
    );
  }
  
}
